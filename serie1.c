#include <stdio.h>

void serie1_ex1() {
    /**
     * Comme dans tout bon cours d'introduction, il est d'usage de faire un "Hello world!"
     * afin de vous présenter le langage.
     *
     * Afin de nous assurer que votre environnement de programmation veuillez essayer d'exécuter cette fonction
     * en indiquant que vous désirez exécuter l'exercice #1
    */
    printf("Hello World!\n");
}

void serie1_ex2()
{
    /*
     * complétez cette fonction afin d'afficher "Bonjour" suivit d'un retour de chariot.
     */
     /* enlever cette ligne de commentaire
            ("Bonjour")
        enlever cette ligne de commentaire */
}

void serie1_ex3() {
    /*
     * modifiez ce code afin d'afficher "Bonjour" suivit d'un retour de chariot
     */
    /* enlever cette ligne de commentaire
            system.out.printLn("Bonjour");
      enlever cette ligne de commentaire */
}


void serie1_ex4()
{
    /*
     * modifiez ce code afin d'afficher la question "Entrez un nombre:"
     * ensuite demandez à l'usager d'entrer un entier
     * affichez ensuite le nombre entré.
     */
    /* insérez votre code ici */
}

void serie1_ex5()
{
    /*
     * Corrigez ce code afin qu'il affiche le nombre qui sera fourni en entré.
     */
    int valeur;
    /*enlever cette ligne de commentaire
    scan(valeur);
    enlever cette ligne de commentaire */
    scanf("%i", &valeur);
    printf("%i\n",valeur);
}

void serie1_ex6()
{
    /*
     * Corrigez ce code afin qu'il demander à l'usager d'entrer une chaine de caractères.
     */
    char  v[20];
    printf("Entrez un texte :");

    /* insérez votre code ici */

    printf("La chaine est %s\n",v);
}


void serie1_ex7()
{
    /*
     * Complétez le code suivant afin de déclarer la
     * variable "varA" comme étant du type int
     * et ayant la valeur 1234
     */

    /* enlever cette ligne de commentaire
    ??? varA ????
    println(varA);
    enlever cette ligne de commentaire */


}
