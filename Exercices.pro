TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
        serie1.c \
        serie2.c \
        serie3.c \
        serie4.c

HEADERS += \
    serie1.h \
    serie2.h \
    serie3.h \
    serie4.h
