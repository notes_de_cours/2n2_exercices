#include <stdio.h>



void serie2_ex1()
{
/*
 * Codez une fonction pour qu'elle demande un nombre entier
 * Ensuite affichez si ce nombre est pair ou impair
 * Rappelez-vous de l'opérateur modulo %
 */
    int valeur;
    printf("Entrez un nombre: ");
    // complétez la fonction...
    scanf("%i", &valeur);
    if(valeur%2 == 0) {
        printf("pair\n");
    } else {
        printf("impair\n");
    }

    // ou, avec un ternaire
    char *reponse;
    reponse = valeur%2 == 0 ? "pair" : "impair";
    printf("%s\n", reponse);

    // ou

    printf("%s\n", valeur%2 == 0 ? "pair" : "impair");
}

void serie2_ex2()
{
/*
 * Codez une fonction pour qu'elle demande 2 entiers à l'usager dans varA et varB.
 * Si varA est plus grande que varB, alors échangez la valeur de varA et varB en utilisant une troisième variable.
 * Affichez varA et varB (en d'autres mots, les valeurs sont affichées dans l'ordre croissant)
 */
    int varA, varB, varC;
    printf("entrez un premier entier ");
    scanf("%i", &varA);
    // complétez la fonction ...

    printf("varA = %i et varB = %i \n", varA, varB);
}

void serie2_ex3()
{
/*
 * Codez une fonction calculant la factoriel d'un nombre.
 * On se rappel que la factoriel de n (n!) est 1*2*3*...*n
 * Utilisez une boucle for
 * Vous n'avez pas à gérer les mauvaises valeurs (-1, 0, a ...)
 */

    int varA, factoriel = 1;
    printf("entrez un entier ");
    scanf("%i", &varA);
    // complétez la fonction ...

    printf("factoriel de %i = %i\n", varA, factoriel);
}

void serie2_ex4()
{
/*
 * Refaire la factorielle, mais en utilisant une boucle while
 */
    int varA, factoriel = 1;
    printf("entrez un entier ");
    scanf("%i", &varA);
    // complétez la fonction ...

    printf("factoriel de %i = %i\n", varA, factoriel);
}
