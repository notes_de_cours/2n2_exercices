#include <stdio.h>
#include <string.h>

void serie3_ex1()
{
/*
 * Codez une fonction qui demande un mot à l'usager et qui affiche ce mot à l'envers
 * Vous devez faire une boucle qui parcours la chaine de caractères entrée
 * en partant de la fin jusqu'au début.
 * Vous ne devez pas afficher les caractères null se trouvant à la fin de la chaine
 *
 */

    char chaine[21];

    printf("Entrez un mot (20 caractères maximum): ");
    scanf("%s", chaine);
    // insérez votre code ici


    printf("\n");
}

void serie3_ex2() {
/*
 * Codez une fonction qui demande 10 entiers positifs à l'usager, les entrepose dnas un tableau
 * et qui les affiche du plus petit au plus grand sans trier le tableau (mais vous pouvez le modifier).
 * (truc: les entiers entrés sont tous positifs... donc tous plus grands qu'un négatif)
 */

    int tableau[10];

    for(int i=0; i<10; i++) {
        printf("entrez la valeur : ");
        scanf("%i", &tableau[i]);
    }
    //insérez votre code ici
}

